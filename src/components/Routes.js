import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Home from '../pages/Home'
import FilmDetails from '../pages/FilmDetails'

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/films/:id' exact component={FilmDetails} />
      </Switch>
    )
  }
}

export default Routes
