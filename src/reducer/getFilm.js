import {
  GET_FILM,
  GET_FILM_SUCCESS,
  GET_FILM_FAIL,
  GET_SPECIFIC,
  GET_SPECIFIC_SUCCESS,
  GET_SPECIFIC_FAIL
} from './constants'

export default function getFilm(
  state = {
    getFilmState: {},
    getSpecificFilmState: {}
  },
  action
) {
  switch (action.type) {
    case GET_FILM:
      return { ...state, loading: true }
    case GET_FILM_SUCCESS:
      return { ...state, loading: false, getFilmState: action.payload.data }
    case GET_FILM_FAIL:
      return { ...state, loading: false, getFilmState: action.error.response.data }

    case GET_SPECIFIC:
      return { ...state, loading: true }
    case GET_SPECIFIC_SUCCESS:
      return { ...state, loading: false, getSpecificFilmState: action.payload.data }
    case GET_SPECIFIC_FAIL:
      return { ...state, loading: false, getSpecificFilmState: action.error.response.data }

    default:
      return state
  }
}

export function getFilmDetails() {
  return {
    type: GET_FILM,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
        url: `/films/`
      }
    }
  }
}

export function getSpecificFilmDetails(data) {
  return {
    type: GET_SPECIFIC,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
        url: `/films/${data}`
      }
    }
  }
}
